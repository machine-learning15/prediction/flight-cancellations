#!/bin/bash
# Create service Account with roles: BigQuery Data Owner, BigQuery Job User, Cloud Dataflow Service Agent,
# Compute Instance Admin (v1), Compute Viewer, Dataflow Admin, Dataflow Worker, Storage Admin
PROJECT=$(gcloud config get-value project)
gcloud iam service-accounts create svc_dataflow --project ${PROJECT}
export GOOGLE_APPLICATION_CREDENTIALS=svc_dataflow_creds.json

python preprocessor.py --project ${PROJECT} --airport_locations gs://${PROJECT}-mixed/airport_locations.csv --airline_names gs://${PROJECT}-mixed/airlines.csv