#!/bin/bash

BUCKET=$1
PROJECT=$(gcloud config get-value project)

bq --project_id=$PROJECT query --destination_table "airlines_raw.filtered_flights_sample" --replace --nouse_legacy_sql "SELECT * FROM airlines_raw.flights_filtered WHERE RAND() < 0.00001"

bq --project_id=$PROJECT extract --destination_format=NEWLINE_DELIMITED_JSON \
   airlines_raw.flights_sample  gs://${BUCKET}-mixed/flights_sample.json

gsutil cp gs://${BUCKET}-mixed/flights_sample.json .