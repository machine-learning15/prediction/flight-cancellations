## Processing data with Apache Beam and Deployment to GCP Dataflow

Ingested data is enriched with external location and timezone data, and data fixes are applied when necessary.  Then it is uploaded to a new table in BigQuery using Apache Beam framework. Job is deployed using GCP Dataflow service.

### Dataset
Ingested dataset contains flights since 2015 January.
- Total row count 41,331,148
- Data size = 15.62 GB

### Data Enrichment and Fixes
- Latitude, longitude and timezone for each airport is added. We constructed a dictionary for all airports and their lat, lon using external APIs.
- Arrival and departure times for origin and destination airports are localized and normalized.  Instances where arrival time is earlier than the departure time are fixed. 
- Cancellation flag for cancelled flights are fixed.
- Carrier names, extracted from BTS, are added. 

### Dataflow
- Google Dataflow service allows deploying distributed Apache Beam jobs in cloud. The cloud compute instances are created based on the job dynamically, and these resources are destroyed after the job completes by the platform.
- It is possible to run the DataFlow job with the default service account which has extensive permissions. I created a custom service account with limited permissions just enough to run the job, then deployed the job from my local environment using this service account.

For insfrastructure and implementation details, see [preprocess/dataflow.sh](preprocess/dataflow.sh) and [preprocess/preprocessor.py](preprocess/preprocessor.py)

### Result DAG
The job took about 45 minutes using through-put based resource allocation algorithm.

<img src="dag.png" width="400" height="400">


