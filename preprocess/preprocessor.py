import csv
import logging
from datetime import datetime

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions

INPUT_DATETIME_FORMAT = '%Y-%m-%d'
OUTPUT_DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S%z"
CANCELLATION_CODES = {
    "A": "CARRIER CAUSED",
    "B": "WEATHER",
    "C": "NATIONAL AVIATION SYSTEM",
    "D": "SECURITY"}
NO_TIMEZONE_FLAG = "TIMEZONE"
EXPECTED_HHMM_LENGTH = 4


def add_timezone(lat, lon):
    try:
        import timezonefinder
        tf = timezonefinder.TimezoneFinder()
        lat = float(lat)
        lon = float(lon)
        return lat, lon, tf.timezone_at(lng=lon, lat=lat)
    except ValueError as e:
        logging.log(level=logging.WARN, msg=f"Unable to find the timezone for lat={lat},lon={lon}, Error={e}")
        return lat, lon, NO_TIMEZONE_FLAG


def format_date(flight_date: datetime.date, hhmm: str, tzone: str) -> str:
    """
    Returns localized date with UTC offset
    """
    import datetime
    import pytz
    try:
        local_timezone = pytz.timezone(tzone)
        # To prevent error like '2016-10-11 24:00:00' does not match format '%Y-%m-%d %H:%M:%S'
        # Parsing date first, then adding hours and minutes
        dt = datetime.datetime.strptime(flight_date, INPUT_DATETIME_FORMAT)
        local_datetime = local_timezone.localize(dt)
        local_datetime += datetime.timedelta(hours=int(hhmm[:2]), minutes=int(hhmm[2:]))

        return local_datetime.strftime(OUTPUT_DATETIME_FORMAT)
    except Exception as e:
        logging.log(logging.ERROR, f"flight_date={flight_date}, hhmm={hhmm}, tzone={tzone}, E = {e}")

def add_24h_if_before(arr_tmstmp, dep_tmstmp):
    """Fix if arrival time < departure time"""
    import datetime
    if arr_tmstmp is not None and dep_tmstmp is not None:
        arr_dt = datetime.datetime.strptime(arr_tmstmp, OUTPUT_DATETIME_FORMAT)
        dep_dt = datetime.datetime.strptime(dep_tmstmp, OUTPUT_DATETIME_FORMAT)
        if arr_dt < dep_dt:
            logging.log(logging.DEBUG, msg=f"Arrival Time is earlier than departure time! Arrival: {arr_tmstmp}, dep = {dep_tmstmp}")
            arr_dt += datetime.timedelta(hours=24)
            return arr_dt.strftime(OUTPUT_DATETIME_FORMAT)

def add_location_meta(fields: dict, airport: dict):
    """
    Adds latitude and longitude for each airport
    Formats Departure and Arrival minutes to full localized date time format.
    """
    # Latitude and longitude for both airports
    fields["DEP_AIRPORT_LAT"] = None
    fields["DEP_AIRPORT_LON"] = None
    fields["ARR_AIRPORT_LAT"] = None
    fields["ARR_AIRPORT_LON"] = None
    try:
        fields["DEP_AIRPORT_LAT"] = airport[fields["ORIGIN_CITY"]][0]
        fields["DEP_AIRPORT_LON"] = airport[fields["ORIGIN_CITY"]][1]
        fields["ARR_AIRPORT_LAT"] = airport[fields["DEST_CITY"]][0]
        fields["ARR_AIRPORT_LON"] = airport[fields["DEST_CITY"]][1]
    except KeyError as e:
        logging.log(level=logging.WARN,
                    msg=f"Unable to find lat,lon for given airport/city={fields['ORIGIN_CITY']}/{fields['DEST_CITY']},Error={e}")

    # Localize with Timezone
    dep_timezone = airport[fields["ORIGIN_CITY"]][2]
    arr_timezone = airport[fields["DEST_CITY"]][2]

    # Convert to string for json schema
    fields['FLIGHT_DATE'] = str(fields['FLIGHT_DATE'])

    if dep_timezone != NO_TIMEZONE_FLAG and len(fields["CRS_DEP_TIME"]) == EXPECTED_HHMM_LENGTH:
        fields["CRS_DEP_TIME"] = format_date(fields["FLIGHT_DATE"], fields["CRS_DEP_TIME"], dep_timezone)
    else:
        fields["CRS_DEP_TIME"] = None

    if arr_timezone != NO_TIMEZONE_FLAG and len(fields["CRS_ARR_TIME"]) == EXPECTED_HHMM_LENGTH:
        fields["CRS_ARR_TIME"] = format_date(fields["FLIGHT_DATE"], fields["CRS_ARR_TIME"], arr_timezone)
    else:
        fields["CRS_ARR_TIME"] = None

    # Handle Cancelled Flights
    cancel_code: str = fields["CANCEL_CODE"]
    if cancel_code is None or len(cancel_code.strip()) == 0:
        if dep_timezone != NO_TIMEZONE_FLAG and len(fields["DEP_TIME"]) == EXPECTED_HHMM_LENGTH:
            fields["DEP_TIME"] = format_date(fields["FLIGHT_DATE"], fields["DEP_TIME"], dep_timezone)
        else:
            fields["DEP_TIME"] = None
        if arr_timezone != NO_TIMEZONE_FLAG and len(fields["ARR_TIME"]) == EXPECTED_HHMM_LENGTH:
            fields["ARR_TIME"] = format_date(fields["FLIGHT_DATE"], fields["ARR_TIME"], arr_timezone)
        else:
            fields["ARR_TIME"] = None

    else:
        # Fix Cancelled flag
        fields["CANCELLED"] = True
        fields["CANCEL_CODE"] = CANCELLATION_CODES[fields["CANCEL_CODE"]]
        fields["DEP_TIME"] = None
        fields["ARR_TIME"] = None

    # Fix instances where arrival time < departure time
    fields["CRS_ARR_TIME"] = add_24h_if_before(arr_tmstmp=fields["CRS_ARR_TIME"], dep_tmstmp=fields["CRS_DEP_TIME"])
    fields["ARR_TIME"] = add_24h_if_before(arr_tmstmp=fields["ARR_TIME"], dep_tmstmp=fields["DEP_TIME"])

    yield fields


def carrier_name(fields: dict, airline_names: dict):
    """
    Add airline commercial name
    """
    unique_carrier: str = fields["UNIQUE_CARRIER"]
    try:
        fields["UNIQUE_CARRIER_NAME"] = airline_names[unique_carrier]
    except KeyError as e:
        fields["UNIQUE_CARRIER_NAME"] = None
        logging.log(level=logging.WARN,
                    msg=f"Unable to find airline name for airline={fields['UNIQUE_CARRIER_NAME']}, Error = {e}")

    yield fields


def extract(line: str):
    if len(line.strip()) == 0 or len(line.split("|")) < 2:
        logging.log(level=logging.WARN, msg=f"Malformed file format. Line = {line}")
    else:
        tokens = line.split("|")
        return tokens[0], tokens[1]


def run(project, airport_locations, airline_names):

    beam_options = PipelineOptions(
        runner='DataflowRunner',
        project=project,
        job_name=f'preprocessflights{datetime.now().strftime("%Y%m%d%H%M")}',
        save_main_session=True,
        temp_location=f'gs://{project}-apachebeam/temp',
        staging_location=f'gs://{project}-apachebeam/staging',
        region="us-west1",
        max_num_workers=8,
        autoscaling_algorithm="THROUGHPUT_BASED",
        requirements_file="requirements.txt")

    with beam.Pipeline(options=beam_options) as pipeline:

        airports = (pipeline
                    | "airports:read" >> beam.io.ReadFromText(airport_locations)
                    | "airports:csv" >> beam.Map(lambda line: next(csv.reader([line])))
                    | "airports:timezone_loc" >> beam.Map(lambda line: (line[0], add_timezone(line[1], line[2])))
                    )

        airlines = (pipeline
                    | "airlines:read" >> beam.io.ReadFromText(airline_names)
                    | "airlines:names" >> beam.Map(extract)
                    )

        flights = (pipeline
                   | 'flights:read' >> beam.io.ReadFromBigQuery(
                    query="SELECT * FROM airlines_raw.flights_filtered ", use_standard_sql=True)
                   | 'flights:add_location' >> beam.FlatMap(add_location_meta, beam.pvalue.AsDict(airports))
                   | "flights:carrier_name" >> beam.FlatMap(carrier_name, beam.pvalue.AsDict(airlines))
                   )

        flights_schema = ",".join(
            ['FLIGHT_DATE:string',
             'UNIQUE_CARRIER:string',
             'ORIGIN_AIRPORT_SEQ_ID:string',
             'ORIGIN:string',
             'ORIGIN_CITY:string',
             'ORIGIN_STATE:string',
             'DEST_AIRPORT_SEQ_ID:string',
             'DEST:string',
             'DEST_CITY:string',
             'DEST_STATE:string',
             'CRS_DEP_TIME:string',
             'DEP_TIME:string',
             'DEP_DELAY_MIN:float',
             'CRS_ARR_TIME:string',
             'ARR_TIME:string',
             'ARR_DELAY_MIN:float',
             'CANCELLED:boolean',
             'CANCEL_CODE:string',
             'DIVERTED:boolean',
             'DISTANCE_MILES:float',
             "CARR_DELAY_MIN:float",
             "WEATHER_DELAY_MIN:float",
             "NAS_DELAY_MIN:float",
             "SEC_DELAY_MIN:float",
             "LATE_AIRCRAFT_DELAY_MIN:float",
             "DEP_AIRPORT_LAT:float",
             "DEP_AIRPORT_LON:float",
             "ARR_AIRPORT_LAT:float",
             "ARR_AIRPORT_LON:float",
             "UNIQUE_CARRIER_NAME:string"])

        (flights
         | "flight:bq" >> beam.io.WriteToBigQuery(table="flights_processed", dataset="airlines_raw",
                                                  schema=flights_schema,
                                                  write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE,
                                                  create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED
                                                  )
         )


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description='Apache Beam job to preprocess raw flights data from BTS.')
    parser.add_argument('--project', help='Unique project ID', required=True)
    parser.add_argument('--airport_locations', help='GCS path to extract airport names', required=True)
    parser.add_argument('--airline_names', help='GCS path to extract airport lat,lon', required=True)

    args = parser.parse_args()

    run(project=args.project, airport_locations=args.airport_locations, airline_names=args.airline_names)
    logging.log(logging.INFO, "Finished processing...")
