import geopy
from geopy.geocoders import Nominatim
import time

geolocator = Nominatim(user_agent="mars.works.airports")
all_cities = set()

# Read all Origin and Destination cities in memory
with open('airport_cities.csv', 'r') as reader:
    lines = reader.readlines()
    for line in lines:
        city = line.strip()
        all_cities.add(city)

results = []
not_found = []
print(f"Processing {len(all_cities)} cities")
with open("airport_locations.csv", "w") as writer:
    for city in all_cities:
        try:
            time.sleep(1)
            loc = None
            if len(city.strip()) > 0:
                loc = geolocator.geocode(city)
                if loc is None:
                    # Handle city/city, state
                    tokens = city.split("/")
                    if len(tokens) > 1:
                        # try with last city and state
                        loc = geolocator.geocode(tokens[-1])
                        if loc is None:
                            # try first city and state
                            c = tokens[0]
                            s = tokens[-1].split(",")[-1] .strip()
                            loc = geolocator.geocode(f"{c},{s}")
            if loc is None:
                not_found.append(city)
                continue
            else:
                lat, lon = loc.latitude, loc.longitude
                writer.write(f"{city},{lat},{lon}\n")
        except Exception as e:
            print(f"Unable to retrieve {city}, Error = {e}")
            not_found.append(city)

print(f"Below locations are not found:\n{not_found}")
