# Flight Cancellations - WIP
 <img src="./img/cancelled.jpeg" alt="cancel" width="150">  Imagine a case where you have an important appointment in another city, so you get a flight ticket for a date and time to make sure you arrive at your appointment on time.  Then the flight gets cancelled, and you find yourself hunting for flight tickets perhaps at the last minute, stressing yourself out and/or paying more for a last-minute ticket and hotel rooms.  Even worse, the flight gets cancelled at the last minute and you end up missing your very important meeting; making a bad impression on potential business partners.  

Is there a way to prevent this from happening? **No, we cannot prevent this from happening, but maybe we can minimize the risk.** If we can rank the available flights according to their probability of cancellation, then we can choose the flight with the least cancellation risk. In the long run, if you are a frequent flier for important meetings, this could make a big difference on your bottomline.

How much difference could this make? Let's say 2 out of every 4 meetings you make on time, you win 5000\$. But if you miss a meeting, you can only reschedule it 1 out of 4 times. If you try to reschedule it proactively, your chance of rescheduling increases to 3 out of 4. So what is your cost of missing a flight comparative to cost of proactively rescheduling it? 

Assume cost of scheduling a meeting is constant. Then, your expected gain if you miss meeting: 1/4 * 1/2 * 5000\$ = 625\$. Expected gain if you reschedule it proactively: 3/4 * 1/4 * 5000\$ = 937\$. EG if you make the meeting on time without having to reschedule it: 1/4 * 5000\$ = 1250\$ . So what we want is, proactively reschedule a meeting if the flight will be cancelled, and do not reschedule if a flight won't be cancelled. And avoid missing meetings.  Easy to say, hard to achieve. We will build a predictive model, capable of predicting a probability of cancellation, and would test it in business settings to see how effective it is in **maximizing our expected gain.**

## Data
Data is retrieved from [Bureau of Transportation Statistics](https://www.transtats.bts.gov/DL_SelectFields.asp?gnoyr_VQ=FGJ&QO_fu146_anzr=b0-gvzr)' servers. For each flight, it contains an indicator for cancellation, in addition to scheduled/actual departure and arrival times.  Data is reported by certified U.S air carriers that account for at least 1% of domestic scheduled passenger revenues. You can find more information on data and ingestion [data ingestion code](ingestion). To see how we processed and validated data, go to [preprocessing code](preprocess). For EDA and data validation, see [eda](eda).

## Modeling risk
We will compare Logistic Regression and Feed forward Neural Net performance and select the best model based on log loss. Both of these models are capable for producing probability score. To see which and how, see [model development and test code](models).

## Application
Given some attributes such as an airline, flight date, departure and arrival airport, the model will return a predicted probability. Returned probability will reflect the model's confidence level for cancellation, not an actual probability. It is best used for ranking multiple flights.

## Findings so far 
<img src="./img/cancels.jpeg" alt="cancel" width="1000"> 

When the pandemic hit USA in March, more than 200,000 flights were cancelled in the first 2 months.
In March 2020, airlines scheduled more than 600,000 flights, where 19% of them were cancelled. Although the number of scheduled flights decreased by about 60% in April 2020, 42% of them still got cancelled. In May 2020, scheduled flights decreased even more down under 200,000 flights, and cancellation rate was down to 6%.
After May 2020, cancellation rate approaches to pre-pandemic level, where number of scheduled flights keeps increading gradually. We see another spike in cancellation rate in February, 2021.

<img src="./img/airlines.png" alt="airlines" width="1000"> 

Above visualization shows some of the airlines' market share and cancel rates from 2018 to March 2020. We see a great variation in cancellation rates among airlines.  Small market share doesn't necessarily imply small cancel rates, and big market share doesn't imply small cancel rates.  

## Results so far
- Train data size: 15,799,275
- Validation data size = 4,771,452
- Proportion of cancellations in train dataset = 0.017

| Model               | Validation log loss | Weight |      Threshold |         Recall          | Precision |
|---------------------|:-------------------:|:------:|---------------:|:-----------------------:|:---------:|
| Baseline            |   0.066             |   NA   |          0.017 |           1.0           |   0.01    |
| Logistic Regression |   0.889             |  100   |            0.5 |          0.98           |   0.012   |

## Next steps
- Neural Network
- Tuning
- Calibration



