import gzip
import shutil
import tempfile
from pathlib import Path
from string import Template
import time
from concurrent import futures
import argparse

import httpx
from google.cloud import storage
from google.cloud import bigquery


class GCS:

    def __init__(self):
        self.storage_client = storage.Client()

    def upload_from_file(self, file_path: str, bucket_name: str, object_name: str) -> str:
        bucket = self.storage_client.bucket(bucket_name)
        blob = bucket.blob(object_name)
        blob.upload_from_filename(file_path)
        if blob.time_created:
            return f"{object_name}, upload status = OK"
        else:
            return f"{object_name}, upload status = Failed"

    def file_exists(self, bucket_name: str, object_name: str) -> bool:
        bucket = self.storage_client.bucket(bucket_name)
        blob = bucket.blob(object_name)

        return blob.exists()

    def get_files(self, bucket_name: str) -> set:
        blobs = self.storage_client.list_blobs(bucket_name)
        return {blob.name for blob in blobs}


class BQLoader:

    def __init__(self, project_id:str, dataset_id:str, table_id:str, bucket_id:str, gcs:GCS):
        self.table_id = f"{project_id}.{dataset_id}.{table_id}"
        self.bucket_id = bucket_id
        self.bq_client = bigquery.Client()
        self.gcs_client = gcs
        self.gcs_file_pattern = Template("gs://${bucket_id}/${year}_${month}.gz")
        self.check_exists = Template(
            "SELECT Year, Month FROM ${table_id} WHERE Year='${year}' and Month='${month}' limit 1")

    def data_exists(self, year, month) -> bool:
        query = self.check_exists.substitute(table_id=self.table_id, year=year, month=month)
        jq = self.bq_client.query(query)
        if jq.errors:
            raise jq.exception()
        result = jq.result()
        return result.total_rows > 0

    def extract(self, name):
        """Extracts year and month from blob name"""
        year = name.replace(".gz", "").split("_")[0]
        month = name.replace(".gz", "").split("_")[1]
        return year, month

    def batch_load(self):
        """Loads all files in the given bucket to the table, if the data doesn't exist in BQ."""
        names = self.gcs_client.get_files(self.bucket_id)
        print(f"Will upload {len(names)} files to BigQuery")
        t0 = time.perf_counter()
        transfer_results = {"Failed": [], "Succeeded": []}

        for name in names:
            year, month = self.extract(name)
            print(f"Processing {year}, {month}")
            try:
                result = self.load(year, month)
                transfer_results["Succeeded"].append((name, result))
            except bigquery.exceptions.BigQueryError as e:
                transfer_results["Failed"].append((name, e))

        elapsed = time.perf_counter() - t0
        print(
            f"Transferred {len(transfer_results['Succeeded'])}, Failed = {len(transfer_results['Failed'])} in {elapsed:.2f} seconds")
        return transfer_results

    def load(self, year, month):
        """Append GCS file to the partitioned table, if the data doesn't exist in the table. """
        uri = self.gcs_file_pattern.substitute(bucket_id=self.bucket_id, year=year, month=month)
        if not self.data_exists(year, month):
            job_config = bigquery.LoadJobConfig()
            job_config.source_format = 'CSV'
            job_config.ignore_unknown_values = True
            job_config.skip_leading_rows = 1

            # truncate existing partition ...
            load_job = self.bq_client.load_table_from_uri(uri, self.table_id, job_config=job_config)
            load_job.result()

            if load_job.state != 'DONE':
                raise load_job.exception()

            return f"{uri} transfer to BQ OK"
        else:

            return f"{uri} transfer skipped"


class Ingestor:

    def __init__(self, gcs: GCS, bucket_name):
        self.compressed_file_pattern = Template(
            "https://transtats.bts.gov/PREZIP/On_Time_Reporting_Carrier_On_Time_Performance_1987_present_${year}_${month}.zip")
        self.uncompressed_file_pattern = Template(
            "On_Time_Reporting_Carrier_On_Time_Performance_(1987_present)_${year}_${month}.csv")
        self.gcs = gcs
        self.bucket_name = bucket_name

    def ingest_file(self, year: int, month: int) -> str:
        url = self.compressed_file_pattern.substitute(year=year, month=month)
        object_name = f"{year}_{month}.gz"

        if self.gcs.file_exists(self.bucket_name, object_name):
            return f"{object_name}, upload status = Already Exists."
        else:
            with tempfile.TemporaryDirectory() as tmpdirname:
                try:
                    # BTS SSL cert does not exist for downloads
                    response = httpx.get(url, follow_redirects=True, verify=False, timeout=120)
                except httpx.HTTPError as e:
                    return f"{object_name}, HTTP error = {e}"

                if response.status_code != 200:
                    return f"{object_name}, Unable to retrieve file = {response.status_code}"

                else:
                    compressed_path = str(Path(tmpdirname, f"{year}_{month}").with_suffix(".zip"))

                    with open(compressed_path, 'wb') as zip_f:
                        zip_f.write(response.content)

                    shutil.unpack_archive(compressed_path, tmpdirname)
                    uncompressed_path = str(
                        Path(tmpdirname, self.uncompressed_file_pattern.substitute(year=year, month=month)))

                    # gzip file as some files are more than 250 MB, and expensive to transfer over network
                    gzip_file_path = str(Path(tmpdirname, f"{year}_{month}").with_suffix(".gz"))
                    with open(uncompressed_path, "rb") as f_in:
                        with gzip.open(gzip_file_path, "wb") as f_out:
                            shutil.copyfileobj(f_in, f_out)

                    return self.gcs.upload_from_file(file_path=gzip_file_path, object_name=object_name,
                                                     bucket_name=self.bucket_name)

    def ingest_all(self, years: list[int]) -> None:
        months = [month for month in range(1, 13)]
        t0 = time.perf_counter()
        with futures.ThreadPoolExecutor(len(years) * 12) as executor:
            results: list[futures.Future] = []
            for year in years:
                for month in months:
                    future = executor.submit(self.ingest_file, year, month)
                    results.append(future)
                    print(f"Scheduled for {year}_{month}.csv")

            for count, future in enumerate(futures.as_completed(results), 1):
                res: str = future.result()
                print(f"Result = {res}")

        elapsed = time.perf_counter() - t0
        print(f"Downloaded {len(years) * 12} files in {elapsed:.2f} seconds")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Ingests Airlines data from BTS website to GCS Object Storage.")
    parser.add_argument("--project", type=str, required=True, help="Project id")
    parser.add_argument("--dataset", type=str, required=True)
    parser.add_argument("--table", type=str, required=True)
    parser.add_argument("--bucket", type=str, required=True, help="Bucket to download the raw data from BTS.")
    parser.add_argument("--years", nargs="+", help="Sequence of 1 or more years given in YYYY format.")
    args = parser.parse_args()

    project = args.project
    dataset = args.dataset
    table = args.table
    bucket = args.bucket
    years = args.years

    # Init
    gcs = GCS()
    ingestor = Ingestor(gcs=gcs, bucket_name=bucket)
    bq_loader = BQLoader(project, dataset, table, bucket, gcs)

    # Ingest
    t0 = time.perf_counter()
    ingestor.ingest_all(years=years)
    transfer_results = bq_loader.batch_load()
    print(transfer_results)
    elapsed = time.perf_counter() - t0
    print(f"Downloaded {len(years) * 12} files in {elapsed:.2f} seconds")
