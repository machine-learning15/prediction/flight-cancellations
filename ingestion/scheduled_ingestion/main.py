import logging
import os
from datetime import date, timedelta

from flask import Flask, request, escape
from ingest import BQLoader
from ingest import GCS
from ingest import Ingestor

table_id = os.getenv("TABLE_ID")
bucket_id = os.getenv("BUCKET")
project_id = os.getenv("PROJECT_ID")
dataset_id = os.getenv("DATASET_ID")

logging.info(f"bucket = {bucket_id}, project = {project_id}, dataset = {dataset_id}, table = {table_id}")
gcs = GCS()
bq = BQLoader(project_id=project_id, dataset_id=dataset_id, table_id=table_id, bucket_id=bucket_id, gcs=gcs)
ingestor = Ingestor(gcs, bucket_id)

app = Flask(__name__)

def get_files(last_months=3):
    today = date.today()
    begin = today - timedelta(weeks=last_months * 4 + 1)
    current_date = begin
    year_month = []
    while ((current_date.year < today.year) or \
           (current_date.year == today.year and current_date.month < today.month)):
        year_month.append((current_date.year, current_date.month))
        current_date = current_date + timedelta(weeks=4)
    return year_month

@app.route("/ingest", methods=['POST'])
def ingest_flights():
    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

    request_json = request.get_json(force=True)
    month: int = int(request_json['month']) if 'month' in request_json else None
    if not month:
        return "Missing month", 400
    else:
        logging.info(f"Ingesting data for the last {month} months...")
        dates = get_files(last_months=month)
        logging.debug(f"Last {month} months files = {dates}")
        for d in dates:
            year, month = d[0], d[1]
            if not gcs.file_exists(bucket_id, f"{year}_{month}.gz"):
                r = ingestor.ingest_file(year, month)
                if "OK" in r:
                    bq.load(year, month)

        return f"Processed {dates}", 200

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))
