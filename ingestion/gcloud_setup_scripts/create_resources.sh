#!/bin/bash
# Create service account and setup access for the bucket and bigquery table
# brew install --cask google-cloud-sdk
# gcloud config list
# gcloud config set project PROJECT_ID
# gcloud config set compute/zone us-west1-c


PROJECT_ID=$(gcloud config get-value project)
DATASET_ID=$1
TABLE_ID=$2
BUCKET=$3
SERVICE_ACCOUNT_NAME=svc-airlines-ingest
REGION=us-west1
SVC_PRINCIPAL=serviceAccount:${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com


########## 1. Create service account for the web service
gcloud iam service-accounts create $SERVICE_ACCOUNT_NAME --display-name "flights monthly ingest"

########## 2. Setup GCS bucket access
gsutil ls gs://$BUCKET || gsutil mb -l $REGION gs://$BUCKET
gsutil uniformbucketlevelaccess set on gs://$BUCKET
gsutil iam ch ${SVC_PRINCIPAL}:roles/storage.admin gs://$BUCKET

########## 3. Create BigQuery table and add web service 'service account' as data owner
bq mk --dataset --location=$REGION --description "Airlines On-time Performance Dataset" ${PROJECT_ID}:${DATASET_ID}
# Create table with partitioning
bq mk --table  --description "Airlines on-time performance raw data" --schema schema.json --time_partitioning_field "FlightDate" --time_partitioning_type "MONTH" ${PROJECT_ID}:${DATASET_ID}.${TABLE_ID}
# Get current permissions on the dataset
bq show --format=prettyjson $PROJECT_ID:$DATASET_ID > dataset.json
# Review and add service account email as the data owner, then update as below
bq update --source dataset.json $PROJECT_ID:$DATASET_ID

########## 4. Test Service Account
# Get service account key, and add it to .gitignore
gcloud iam service-accounts keys create credentials.json --iam-account ${SERVICE_ACCOUNT_NAME@${PROJECT_ID}.iam.gserviceaccount.com
export GOOGLE_APPLICATION_CREDENTIALS=credentials.json
bash ingest.py --project $PROJECT_ID --dataset $DATASET_ID --table $TABLE_ID --bucket $BUCKET --years 2015 2016

########## 5. Create Cloud Run Service
CLOUD_RUN_NAME=flights-monthly-ingest
gcloud run deploy ${CLOUD_RUN_NAME} --image gcr.io/${PROJECT_ID}/flights_ingest:latest \
 --set-env-vars "PROJECT_ID=${PROJECT_ID}" \
 --set-env-vars "DATASET_ID=${DATASET_ID}" \
 --set-env-vars "TABLE_ID=${TABLE_ID}" \
 --set-env-vars "BUCKET=${BUCKET}" \
 --min-instances 0 --region $REGION \
 --platform=managed --timeout 12m \
 --service-account ${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com --no-allow-unauthenticated
# endpoint is output

# Create service account to invoke cloud run
CLOUDRUN_SERVICE_ACCOUNT_NAME=svc-cloud-invoke
gcloud iam service-accounts create $CLOUDRUN_SERVICE_ACCOUNT_NAME --project $PROJECT_ID
gcloud iam service-accounts keys create credentials.json --iam-account ${CLOUDRUN_SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com

# Add the service account to the cloud run as an invoker
gcloud projects add-iam-policy-binding ${PROJECT_ID} --member serviceAccount:${CLOUDRUN_SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com --role roles/run.invoker
# Test invoking the endpoint with the service account
gcloud auth activate-service-account --key-file credentials.json
curl -d "{'month': 6}" -H "Content-Type: application/json" -H "Authorization: Bearer $(gcloud auth print-identity-token)" -X POST  [endpoint]/ingest

########## 6. Create Scheduler
gcloud auth login
SCHEDULER_NAME=flights-monthly-ingest-job
SVC_URL=$(gcloud run services describe flights-monthly-ingest --format 'value(status.url)')/ingest
echo $SVC_URL
echo $SVC_EMAIL

# Ingest last 3 month data
echo {\"month\":\"3\"\} > /tmp/message
cat /tmp/message

gcloud scheduler jobs create http $SCHEDULER_NAME \
--description "Ingest flights using Cloud Run" \
--schedule="8 of month 10:00" --time-zone "Etc/UTC" \
 --uri=$SVC_URL --http-method POST \
 --oidc-service-account-email $SVC_EMAIL --oidc-token-audience=$SVC_URL \
 --max-backoff=7d \
 --max-retry-attempts=3 \
 --max-retry-duration=2d \
 --min-backoff=12h \
 --headers="Content-Type=application/json" \
 --message-body-from-file=/tmp/message

# In cloud scheduler, Run Now to test.
